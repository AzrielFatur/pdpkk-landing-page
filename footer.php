<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="container-fluid footer">

		<div class="<?php echo esc_attr( $container ); ?>">

			<div class="row justify-content-center text-center">

				<!-- <?php understrap_site_info(); ?> -->
				<!-- <div class="col-lg-4 col-md-12 col-12 contact-us">
					<h4>Information Us</h4>
					<p>About</p>
					<p>Contact</p>
				</div> -->
				<div class="col-lg-4 col-md-12 col-12 follow-us">
					<h4>I N F O R M A T I O N</h4>
					<p>About Us</p>
					<p>Contact Us</p>
					<p>Blog</p>
					<!-- <p><i class="fab fa-facebook-f"></i> <i class="fab fa-instagram"></i> <i class="fab fa-youtube"></i></p> -->
				</div>
				<div class="col-lg-4 col-md-12 col-12 contact-us">
					<h4>C O N T A C T</h4>
					<p>pdpkk.laurentiusbdg@gmail.com</p>
					<!-- <p>+62 878 8373 0944</p> -->
					<p><a href="https://www.facebook.com/Pdpkklaurentius"><i class="fab fa-facebook-f"></i></a> <a href="https://www.instagram.com/pdpkk.laurentiusbdg/"><i class="fab fa-instagram"></i></a> <a href="https://www.youtube.com/c/PDPKKSTLAURENTIUSBANDUNGVIDEO"><i class="fab fa-youtube"></i></a></p>

					<!-- <p><i class="fas fa-phone"></i> +62 878 8373 0944</p>
					<p><i class="far fa-envelope"></i> pdppk@gmail.com</p> -->
				</div>
				<div class="col-lg-4 col-md-12 col-12">
					<h4>A D D R E S S</h4>
					<p>Gereja St. Laurentius. <br>Jl. Sukajadi No.223, Gegerkalong, Kec. Sukasari, Kota Bandung, Jawa Barat 40153</p>
				</div>

				<div class="col-lg-12 mt-3">
					<p>Copyright © 2020 PDPPK St. Laurentius Bandung All Right Reserved</p>
				</div>

			</div><!-- row end -->

		</div><!-- container end -->

	</div>

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

