<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$container = get_theme_mod( 'understrap_container_type' );
?>

<?php if ( is_front_page() && is_home() ) : ?>
	<?php get_template_part( 'global-templates/hero' ); ?>
<?php endif; ?>

<!-- Section 1 -->
<div class="jumbotron-fluid first-section">
	<div class="full-image" style="background:url('<?php echo get_template_directory_uri(); ?>/images/background-section.jpg') no-repeat top center;background-size:cover;position:relative;height:100vh;width:100%;display: block;">
	<div class="overlay"></div>
	<h1 class="display-4">Be Happy for this moment,<br>This moment is your life.</h1>
	</div>
</div>

<!-- Section 2 -->
<div id="about"></div>
<div class="container second-section">
	<div class="row">
		<div class="col-lg-6 col-md-6 col-12 mt-5">
			<h2>About</h2>
			<p>Persekutuan Doa Karismatik Katolik St. LAURENTIUS Bandung setiap Kamis pk. 19.00 Wib di R. CHRISTA. <br><br>Mari kita memuji & menyembah Tuhan.</p>
			<!-- <h6><a href="" style="color: #ECB540;">Selengkapnya ></a></h6> -->
		</div>	
		<div class="col-lg-6 col-md-6 col-12">
			<img src="<?php echo get_template_directory_uri(); ?>/images/about-section.jpg" width="100%">
		</div>
	</div>
</div>

<!-- Section 3 -->
<div class="container-fluid third-section" style="background:url('<?php echo get_template_directory_uri(); ?>/images/third-section.png') no-repeat top center;background-size:cover;">
	<div class="container">	
		<div class="row text-center">
			<div class="col-lg-4 mt-5">
				<i class="fas fa-award"></i>
				<h4>Pujian</h4>
				<!-- <p>Lorem Ipsum is simply dummy text printing and typesetting industry.</p> -->
			</div>
			<div class="col-lg-4 mt-5">
				<i class="fas fa-user"></i>
				<h4>Pewarta</h4>
				<!-- <p>Lorem Ipsum is simply dummy text printing and typesetting industry.</p> -->
			</div>
			<div class="col-lg-4 mt-5">
				<i class="fas fa-praying-hands"></i>
				<h4>Pendoa</h4>
				<!-- <p>Lorem Ipsum is simply dummy text printing and typesetting industry.</p> -->
			</div>
		</div>
	</div>
</div>

<div class="wrapper fourth-section" id="index-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<!-- Do the left sidebar check and opens the primary div -->
			<?php get_template_part( 'global-templates/left-sidebar-check' ); ?>

			<main class="site-main row" id="main">

				<?php if ( have_posts() ) : ?>

					<?php /* Start the Loop */ ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'loop-templates/content', get_post_format() );
						?>

					<?php endwhile; ?>

				<?php else : ?>

					<?php get_template_part( 'loop-templates/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php understrap_pagination(); ?>

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #index-wrapper -->

<?php get_footer();
